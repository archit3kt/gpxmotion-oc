OC.L10N.register(
    "gpxmotion",
    {
    "View in GpxMotion" : "Zobraziť v GpxMotion",
    "Edit with GpxMotion" : "Upraviť s GpxMotion",
    "no vehicle" : "žiadne vozidlo",
    "plane" : "lietadlo",
    "car" : "auto",
    "foot" : "pešo",
    "bike" : "bicykel",
    "train" : "vlak",
    "bus" : "autobus",
    "Delete" : "Vymazať",
    "Vehicle" : "Vozidlo",
    "Color" : "Farba",
    "Description" : "Popis",
    "Options" : "Možnosti",
    "Legend" : "Vysvetlivky",
    "step" : "krok"
},
"nplurals=4; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 3;");
