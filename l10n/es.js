OC.L10N.register(
    "gpxmotion",
    {
    "Tile server \"{ts}\" has been deleted" : "Servidor de mosaico \"{ts}\" se ha eliminado",
    "Failed to delete tile server \"{ts}\"" : "No se pudo eliminar el servidor de mosaico \"{ts}\"",
    "Impossible to add tile server" : "Imposible añadir servidor de mosaico",
    "Tile server \"{ts}\" has been added" : "Se ha agregado el servidor de mosaico \"{ts}\"",
    "Failed to add tile server \"{ts}\"" : "No se pudo agregar el servidor de mosaico \"{ts}\"",
    "Custom tile servers" : "Servidores de mosaicos personalizados",
    "For example : http://tile.server.org/cycle/{z}/{x}/{y}.png" : "Por ejemplo: http://tile.server.org/cycle/{z}/{x}/{y}.png",
    "Your tile servers" : "Sus servidores de mosaico",
    "Custom overlay tile servers" : "Servidores de mosaicos de superposición personales",
    "Your overlay tile servers" : "Sus servidores de mosaico de superposición",
    "Custom WMS tile servers" : "Servidores de mosaico WMS personalizados",
    "Your WMS tile servers" : "Sus servidores de mosaico WMS",
    "Your WMS overlay tile servers" : "Sus servidores de mosaico de superposición WMS"
},
"nplurals=2; plural=(n != 1);");
